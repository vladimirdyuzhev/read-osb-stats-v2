package com.readosbstats;

import java.util.ArrayList;
import java.util.List;

public class Service implements Comparable<Service> {
	public String name;
	public String href;
	public List<Operation> operations = new ArrayList<Operation>();
	public String type;
	
	public long slaAlertCount;
	public long minResponseTime;
	public long maxResponseTime;
	public long avgResponseTime;
	public long messageCount;
	public long errorCount;	
	public long failoverCount;
	public double successRatio;
	public double failureRatio;
	public long wsSecurityErrors;
	public long validationErrors;
	public long hitCount;
	public double hitRatio;
	public long minThrottlingTime;
	public long maxThrottlingTime;
	public long avgThrottlingTime;
	
	// TODO: make it a list of Endpoint objects with their own counts and response times
	public List<String> endpointUris = new ArrayList<String>();
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public int compareTo(Service o) {
		return name.compareToIgnoreCase(o.name);
	}
}