package com.readosbstats;

public class Operation implements Comparable<Operation> {
	public String name;

	public long messageCount;
	public long errorCount;
	
	public long minResponseTime;
	public long avgResponseTime;
	public long maxResponseTime;
	
	public String debug;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation other = (Operation) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equalsIgnoreCase(other.name))
			return false;
		return true;
	}
	@Override
	public int compareTo(Operation o) {
		return name.compareToIgnoreCase(o.name);
	}
}