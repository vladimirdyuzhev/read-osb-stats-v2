package com.readosbstats;

import java.io.PrintStream;
import java.util.Iterator;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import com.martiansoftware.jsap.UnflaggedOption;

public class ReadStats {

    public static void reportError(JSAP jsap, JSAPResult config) {
        printHeader(System.err);

        @SuppressWarnings("unchecked")
        Iterator<String> emi = config.getErrorMessageIterator();
        while (emi.hasNext()) {
            String next = emi.next();
            System.err.println("  " + next);
            System.err.println();
        }

        System.err.flush();
        System.out.println("To see help, use -h");
    }

    private static void printHeader(PrintStream out) {
        System.err.println("\nRead OSB 11g Statistics");
        System.err.println("(C) Vladimir Dyuzhev, 2015");
        System.err.println("http://readosbstats.com\n");
    }

    public static void reportHelp(JSAP jsap, boolean printHeader) {
        if (printHeader) {
            printHeader(System.out);
        }
        System.out.println("Usage:\n");
        System.out.println("java -jar readosbstats.jar " + jsap.getUsage());
        System.out.println(jsap.getHelp());
    }

    public static void main(String args[]) throws Exception {
        JSAP jsap = new JSAP();

        FlaggedOption optConsole = new FlaggedOption("console")
                .setStringParser(JSAP.URL_PARSER)
                .setShortFlag('c')
                .setLongFlag("console");
        optConsole.setHelp("Admin console URL, e.g.\nhttp://142.113.108.147:7001/sbconsole");
        jsap.registerParameter(optConsole);

        FlaggedOption optUser = new FlaggedOption("user")
                .setStringParser(JSAP.STRING_PARSER)
                .setShortFlag('u')
                .setLongFlag("user");
        optUser.setHelp("Username. If started with @, denotes a file name where credentials are securely stored in username:password format");
        jsap.registerParameter(optUser);

        FlaggedOption optPass = new FlaggedOption("pass")
                .setStringParser(JSAP.STRING_PARSER)
                .setShortFlag('p')
                .setLongFlag("pass");
        optPass.setHelp("Password");
        jsap.registerParameter(optPass);

        Switch optMerge = new Switch("merge")
                .setShortFlag('m')
                .setLongFlag("merge");
        optMerge.setHelp("If specified, all arguments are considered previously saved stats that should be merged into one output CSV file.");
        jsap.registerParameter(optMerge);

        FlaggedOption optWildcard = new FlaggedOption("wildcard")
                .setStringParser(JSAP.STRING_PARSER)
                .setShortFlag('i')
                .setLongFlag("wildcard");
        optWildcard.setHelp("If specified, information will be collected only from services which correspond to specified value.");
        jsap.registerParameter(optWildcard);

        Switch currentPeriod = new Switch("currentperiod")
                .setShortFlag('t')
                .setDefault("false")
                .setLongFlag("currentperiod");
        currentPeriod.setHelp("If specified, information will be collected only for the current period (10 minutes by default).");
        jsap.registerParameter(currentPeriod);


        FlaggedOption optFields = new FlaggedOption("fields")
                .setStringParser(JSAP.STRING_PARSER)
                .setShortFlag('f')
                .setLongFlag("fields");
        optFields.setHelp(
                "Comma-separated list of fields to output:\n" +
                        "msgs: Number of messages\n" +
                        "errs: Number of errors\n" +
                        "min: Minumum response time, ms\n" +
                        "avg: Average response time, ms\n" +
                        "max: Maximum response time, ms\n" +
                        "fovr: Failovers\n" +
                        "succ: Success, %\n" +
                        "fail: Failures, %\n" +
                        "chit: Cache hits\n" +
                        "crat: Cache hits ratio\n" +
                        "mint: Minumum time in throttling queue, ms\n" +
                        "avgt: Average time in throttling queue, ms\n" +
                        "maxt: Maximum time in throttling queue, ms\n" +
                        "Example: -f msgs,errs,avg"
        );
        jsap.registerParameter(optFields);

//        Switch optNoIncrement = new Switch("noincrement")
//     		.setShortFlag('n') 
//     		.setDefault("false")
//     		.setLongFlag("noincrement");
//     	optNoIncrement.setHelp("When merging, output the absolute numbers, not increments.");
//     	jsap.registerParameter(optNoIncrement);

        Switch servicesOnly = new Switch("servicelevel")
                .setShortFlag('s')
                .setDefault("false")
                .setLongFlag("servicelevel");
        servicesOnly.setHelp("Only collect service-level statistics.");
        jsap.registerParameter(servicesOnly);

        Switch nonZeroOnly = new Switch("nonzero")
                .setShortFlag('z')
                .setDefault("false")
                .setLongFlag("nonzero");
        nonZeroOnly.setHelp("Only output resources and operations that have messages.");
        jsap.registerParameter(nonZeroOnly);

        FlaggedOption optOut = new FlaggedOption("output")
                .setStringParser(JSAP.STRING_PARSER)
                .setShortFlag('o')
                .setLongFlag("output");
        optOut.setHelp("Name of output file or directory; if omitted or only directory is specified, the file name is built to include the domain name, hostname:port and timestamp.");
        jsap.registerParameter(optOut);

        Switch optHelp = new Switch("help")
                .setShortFlag('h')
                .setLongFlag("help");
        optHelp.setHelp("Provide this help");
        jsap.registerParameter(optHelp);

        UnflaggedOption optFiles = new UnflaggedOption("file")
                .setStringParser(JSAP.STRING_PARSER)
                .setRequired(false)
                .setGreedy(true);
        optFiles.setHelp("Files (or directories containing files) to merge into one, when -m is specified.");
        jsap.registerParameter(optFiles);

        JSAPResult config = jsap.parse(args);
        if (!config.success()) {
            reportError(jsap, config);
            System.exit(1);
        }
        if (config.getBoolean("help")) {
            reportHelp(jsap, true);
            System.exit(0);
        }

        if (config.getBoolean("merge")) {
            Merge.process(jsap, config);
            return;
        }

//        Scanner.process(jsap, config);
        ScanManager scanManager = new ScanManager();
        scanManager.scan(jsap, config);
    }


}