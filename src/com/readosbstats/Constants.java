package com.readosbstats;

public final class Constants {

    private Constants() {

    }

    public static final String SCANNING_MODE = "scanningMode";
    public static final String WILDCARD = "wildcard";
    public static final String HOST = "host";
    public static final String PORT = "port";

    public static final String NAME = "name";
    public static final String COUNT = "count";
    public static final String ERRORS = "errors";
    public static final String AVG = "avg";
    public static final String MAX = "max";
    public static final String MIN = "min";
}
