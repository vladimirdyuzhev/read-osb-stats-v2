package com.readosbstats;

import java.util.Map;

/**
 * Domain class contains all required
 * information about Weblogic Server domain
 *
 * @version 1.0
 */
public class Domain {

    private String name;

    private DomainVersion domainVersion;

    private Map<String, String> cookies;

    private String consoleUrl;

    public Domain(String name, DomainVersion domainVersion, Map<String, String> cookies, String consoleUrl) {
        this.name = name;
        this.domainVersion = domainVersion;
        this.cookies = cookies;
        this.consoleUrl = consoleUrl;
    }

    public DomainVersion getDomainVersion() {
        return domainVersion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public String getConsoleUrl() {
        return consoleUrl;
    }
}
