package com.readosbstats;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;

public class Merge {

    private static List<File> files = new ArrayList<File>();
    private static Pattern pattern;
    private static List<Snapshot> processed = new ArrayList<Snapshot>();

    public static void process(JSAP jsap, JSAPResult config) throws Exception {
        String wildcard = config.getString("wildcard");
        if (wildcard != null) {
            pattern = buildPattern(wildcard);
        }
        String[] sa = config.getStringArray("file");
        for (String s : sa) {
            File file = new File(s);
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                for (File file2 : files) {
                    addFile(file2);
                }
            } else {
                addFile(file);
            }
        }

        for (File file : files) {
            processFile(file);
        }

        String fn = config.getString("output");
        if (fn == null) {
            fn = "merged.csv";
        }

        if (!processed.isEmpty()) {
            File file = new File(fn);
            if( file.isDirectory() ) {
            	file = new File(file,"merged.csv");
            }
            System.out.println("\nWriting " + file.getAbsolutePath() + " as CSV...");
            Writer.output(processed, file, config);
        }

        System.out.println("Done.");
    }

    private static void addFile(File file) {
        files.add(file);
    }

    private static void processFile(File file) throws Exception {
        String[] markers = new String[]{};
        List<Integer> snapshotStarts = new ArrayList<Integer>();

        System.out.println("Processing " + file.getAbsolutePath() + "...");

        // scanning markers
        // srv,typ,op,#msgs,errs,min,avg,max,fovr,succ,fail,chit,crat,mint,avgt,maxt,
        LineNumberReader lnr = new LineNumberReader(new FileReader(file));
        while (true) {
            String line = lnr.readLine();
            if (line != null && line.startsWith("NOT MERGEABLE")) {
                System.out.println("File " + file.getAbsolutePath() + " could not be merge");
                return;
            }
            if (line == null) break;
            if (line.startsWith("srv,")) {
                // got our markers
                markers = line.split(",", Integer.MAX_VALUE);

                for (int i = 0; i < markers.length; ++i) {
                    if (markers[i].startsWith("#")) {
                        snapshotStarts.add(i);
                    }
                }
            }
        }
        lnr.close();

        // ,,,2015-07-14 11:28,,,,,,,,,,,,,
        // ,,,https://foobar/sbconsole,,,,,,,,,,,,, OR ,,,FOO_BAR,,,
        // Service,Type,Operation,Messages,Errors,Min(ms),Avg(ms),Max(ms),Failovers,Success(%),Failures(%),Cache Hits,Cache Hit(%),Min Throttling(ms),Avg Throttling(ms),Max Throtting(ms),
        lnr = new LineNumberReader(new FileReader(file));

        List<Snapshot> ps = new ArrayList<Snapshot>();

        // skipping the header line
        String dateLine = lnr.readLine();
        if( dateLine == null ) return; // empty file 
        	
        String[] dls = dateLine.split(",", Integer.MAX_VALUE);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        for (Integer st : snapshotStarts) {
            Date date = sdf.parse(dls[st]);
            Snapshot ss = new Snapshot();
            ss.date = date;
            ps.add(ss);
        }
        String urlLine = lnr.readLine();
        dls = urlLine.split(",", Integer.MAX_VALUE);
        int snapn = 0;
        for (Integer st : snapshotStarts) {
            String curl = dls[st];
            Snapshot ss = ps.get(snapn++);
            ss.consoleUrl = curl;
            
            int http = ss.consoleUrl.indexOf("(http");
            if( http > 0 ) {
            	// FOO_BAR(http://foobar/console)
            	ss.domainName = ss.consoleUrl.substring(0,http);
            	ss.consoleUrl = ss.consoleUrl.substring(http+1,ss.consoleUrl.length()-1);
            } else {
            	// FOO_BAR
            	ss.domainName = ss.consoleUrl;
            	ss.consoleUrl = null;
            }
        }

        // skip the human-readable header line
        lnr.readLine();

        String serviceName = "";
        String serviceType = "";

        while (true) {
            String line = lnr.readLine();
            if (line == null) break;
            if (line.trim().isEmpty()) continue;
            if (line.startsWith("srv,")) break; // this is true last line

            String[] values = line.split(",", Integer.MAX_VALUE);

            String name = values[0];
            
            if( !isNameMatches(name) ) continue;

            String type = values[1];
            String operation = values[2];

            for (int sn = 0; sn < ps.size(); ++sn) {
                Snapshot ss = ps.get(sn);

                Service srv = ss.services.isEmpty() ? null : ss.services.get(ss.services.size() - 1);

                String messages = null;
                String errors = null;
                String min = null;
                String avg = null;
                String max = null;
                String failovers = null;
                String success = null;
                String failures = null;
                String cachehits = null;
                String cacheratio = null;
                String mint = null;
                String avgt = null;
                String maxt = null;

                Integer st = snapshotStarts.get(sn);
                int offset = 0;
                while (true) {
                    int i = st + offset;

                    // reached the end of input?
                    if (i >= values.length) break;

                    // reached the end of the snapshot?
                    if (offset != 0 && markers[i].startsWith("#")) break;

                    String val = values[i];
                    String mark = markers[i].replaceAll("#", "");

                    if (Writer.MARKER_AVG.equals(mark)) avg = val;
                    else if (Writer.MARKER_CACHE_HIT.equals(mark)) cachehits = val;
                    else if (Writer.MARKER_CACHE_RATIO.equals(mark)) cacheratio = val;
                    else if (Writer.MARKER_ERRORS.equals(mark)) errors = val;
                    else if (Writer.MARKER_FAILOVER.equals(mark)) failovers = val;
                    else if (Writer.MARKER_FAILURE.equals(mark)) failures = val;
                    else if (Writer.MARKER_MAX.equals(mark)) max = val;
                    else if (Writer.MARKER_MESSAGES.equals(mark)) messages = val;
                    else if (Writer.MARKER_MIN.equals(mark)) min = val;
                    else if (Writer.MARKER_SUCCESS.equals(mark)) success = val;
                    else if (Writer.MARKER_THROTTLING_AVG.equals(mark)) avgt = val;
                    else if (Writer.MARKER_THROTTLING_MAX.equals(mark)) maxt = val;
                    else if (Writer.MARKER_THROTTLING_MIN.equals(mark)) mint = val;

                    offset++;
                }

                if (!serviceName.equals(name) || !serviceType.equals(type)) {
                    // new service
                    srv = new Service();
                    srv.name = name;
                    srv.type = type;

                    if (messages != null) srv.messageCount = parseLong(messages);
                    if (errors != null) srv.errorCount = parseLong(errors);
                    if (min != null) srv.minResponseTime = parseLong(min);
                    if (avg != null) srv.avgResponseTime = parseLong(avg);
                    if (max != null) srv.maxResponseTime = parseLong(max);
                    if (failovers != null) srv.failoverCount = parseLong(failovers);
                    if (success != null) srv.successRatio = parseDouble(success);
                    if (failures != null) srv.failureRatio = parseDouble(failures);
                    if (cachehits != null) srv.hitCount = parseLong(cachehits);
                    if (cacheratio != null) srv.hitRatio = parseDouble(cacheratio);
                    if (mint != null) srv.minThrottlingTime = parseLong(mint);
                    if (avgt != null) srv.avgThrottlingTime = parseLong(avgt);
                    if (maxt != null) srv.maxThrottlingTime = parseLong(maxt);

                    ss.services.add(srv);
                } else {
                    Operation op = new Operation();
                    op.name = operation;
                    srv.operations.add(op);

                    op.messageCount = parseLong(messages);
                    op.errorCount = parseLong(errors);
                    op.minResponseTime = parseLong(min);
                    op.avgResponseTime = parseLong(avg);
                    op.maxResponseTime = parseLong(max);
                }
            }

            serviceName = name;
            serviceType = type;
        }

        processed.addAll(ps);
    }

    private static long parseLong(String messages) {
    	try {
    		return Long.parseLong(messages);
    	} catch( Exception ex ) {
    		return 0;
    	}
	}

    private static Double parseDouble(String messages) {
    	try {
    		return Double.parseDouble(messages);
    	} catch( Exception ex ) {
    		return 0.0;
    	}
	}
    
	private static Pattern buildPattern(String input) {
        input = input.replaceAll("\\*", ".*");
        String r = ".*" + input + ".*";
        return Pattern.compile(r, Pattern.CASE_INSENSITIVE);
    }

    private static boolean isNameMatches(String serviceName) {
        if (pattern == null) {
            return true;
        } else {
            Matcher matcher = pattern.matcher(serviceName);
            return matcher.matches();
        }
    }
}
