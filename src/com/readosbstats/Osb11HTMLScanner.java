package com.readosbstats;

import com.martiansoftware.jsap.JSAPResult;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.readosbstats.Constants.*;

/**
 * Scanner of OSB version 11
 * implemented using HTML pages parsing
 *
 * @version 1.0
 */
public class Osb11HTMLScanner implements IScanner {

    private List<Service> linksToServices = new ArrayList<Service>();
    private List<Service> knownServices = new ArrayList<Service>();


    @Override
    public void getServicesStatistics(Domain domain, List<Service> services, JSAPResult config, Map<String, String> options) throws Exception {
        Collections.sort(services);
        for (Service srv : services) {
            Collections.sort(srv.operations);
        }

        for (Service srv : services) {
            getServiceStatistics(domain, srv, config, options);
        }

        String host = options.get(HOST);
        String port = options.get(PORT);
        String scanningMode = options.get(SCANNING_MODE);

        String fn = config.getString("output");
        if (fn == null) {
            fn = makeFileName(domain, host, port);
        }  
        File file = new File(fn);
        if( file.isDirectory() ) {
        	file = new File(file,makeFileName(domain, host, port));
        }
        
        Snapshot ss = new Snapshot();
        ss.date = new Date();
        ss.consoleUrl = domain.getConsoleUrl();
        ss.domainName = domain.getName();
        ss.services = services;

        System.out.println("\nWriting " + file.getAbsolutePath() + " as CSV...");
        ArrayList<Snapshot> uno = new ArrayList<Snapshot>();
        uno.add(ss);
        Writer.output(uno, file, config, "agg_view".equals(scanningMode));

        System.out.println("Done.");
    }

	private String makeFileName(Domain domain, String host, String port) {
		String fn;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
		fn = "OSB-stats-" + domain.getName() + "(" + host + "-" + port + ")-" + sdf.format(new Date()) + ".csv";
		return fn;
	}

    @Override
    public void getServiceStatistics(Domain domain, Service service, JSAPResult config, Map<String, String> options) throws Exception {
        String scanningMode = options.get(Constants.SCANNING_MODE);
        System.out.println("Processing " + service.name + "...");
        collectService(domain, service, config, scanningMode);
    }

    @Override
    public List<Service> collectAllServices(Domain domain, Map<String, String> options) throws Exception {
        String scanningMode = options.get(SCANNING_MODE);
        String wildcard = options.get(WILDCARD);
        if (wildcard != null) {
            wildcard = buildPattern(wildcard);
        }

        Connection.Response response = getResponse(domain, scanningMode);

        int pageNo = 1; // re-starting from page one to make sure no services are lost
        while (true) {
            System.out.println("\nPAGE " + pageNo + "\n");

            String sh1 = getSh1(pageNo, domain, scanningMode);
            // System.out.println(sh1);
            Document doc = getWithRetry(sh1, domain);
            // System.out.println(doc);
            int n = collectLinks(doc, wildcard);
            if (n == 0) break;

            pageNo++;
        }
        ArrayList<Service> result = new ArrayList<Service>(linksToServices);
        linksToServices.clear();
        knownServices.clear();
        return result;
    }

    private void collectService(Domain domain, Service srv, JSAPResult config, String scanningMode) throws Exception {
        int pageNo = 1;

        boolean serviceLevel = config.getBoolean("servicelevel");

        System.out.println("  Processing service-level stats...");

        // http://142.113.108.147:8001/sbconsole/sbconsole.portal?_nfpb=true&_pageLabel=Monitoring_Service_Statistics&
        // ServiceStatisticsPortletservice_to_monitor=ProxyService%24CustomerProfile%24ProxyService%24AccountDispatch&
        // ServiceStatisticsPortletview_type=agg_view&
        // ServiceStatisticsPortletmanaged_server=&
        // ServiceStatisticsPortletisnew=true
        String sl = getSl(domain, srv, scanningMode);

        Document doc = getWithRetry(sl, domain);
        // System.out.println(doc);

        Elements data = doc.select(".formlabel_normal");

        String key = null;
        for (int i = 0; i < data.size(); ++i) {
            Element element = data.get(i);
            String val = element.text();

            val = val.replaceAll("\\xA0+", "");
            val = val.trim();

            if ("SLA Alert Count".equals(key)) {
                // 2 Alerts ; Warning 7/10/15 3:12 PM
                int sp = val.indexOf(" ");
                if (sp > 0) val = val.substring(0, sp);
                if (val.trim().isEmpty()) val = "0";
                val = val.replaceAll("[^0-9]+", "");
                srv.messageCount = Long.parseLong(val);
            }

            if ("Message Count".equals(key)) {
                val = val.replaceAll("[^0-9]+", "");
                srv.messageCount = Long.parseLong(val);
            }

            if ("Error Count".equals(key)) {
                val = val.replaceAll("[^0-9]+", "");
                srv.errorCount = Long.parseLong(val);
            }

            if ("Overall Avg. Response Time".equals(key)) {
                srv.avgResponseTime = timeToNumber(val);
            }

            if ("Min Response Time".equals(key)) {
                srv.minResponseTime = timeToNumber(val);
            }

            if ("Max Response Time".equals(key)) {
                srv.maxResponseTime = timeToNumber(val);
            }

            if ("Failover Count".equals(key)) {
                val = val.replaceAll("[^0-9]+", "");
                srv.failoverCount = Long.parseLong(val);
            }

            if ("Success Ratio".equals(key)) {
                srv.successRatio = parsePercent(val);
            }

            if ("Failure Ratio".equals(key)) {
                srv.failureRatio = parsePercent(val);
            }

            if ("Hit Count".equals(key)) {
                val = val.replaceAll("[^0-9]+", "");
                srv.hitCount = Long.parseLong(val);
            }

            if ("Hit Ratio".equals(key)) {
                srv.hitRatio = parsePercent(val);
            }

            if ("Min Throttling Time".equals(key)) {
                srv.minThrottlingTime = timeToNumber(val);
            }

            if ("Max Throttling Time".equals(key)) {
                srv.maxThrottlingTime = timeToNumber(val);
            }

            if ("Average Throttling Time".equals(key)) {
                srv.avgThrottlingTime = timeToNumber(val);
            }

            key = val;
        }

        // if service-level only, do not scan operations
        if (serviceLevel) return;

        while (true) {
            System.out.println("  Processing stats page " + pageNo + "...");

            String lead = getLead(domain, srv, pageNo, scanningMode);

            doc = getWithRetry(lead, domain);

            int found = 0;

            int line = 0;
            while (true) {
                Map<String, String> columnsMap = buildOperationColumnsMap(line, scanningMode);
                Elements names = doc.select("#" + columnsMap.get(NAME));
                if (names.isEmpty()) break;

                Elements cnts = doc.select("#" + columnsMap.get(COUNT));
                Elements errs = doc.select("#" + columnsMap.get(ERRORS));
                Elements avgs = doc.select("#" + columnsMap.get(AVG));
                Elements mins = doc.select("#" + columnsMap.get(MIN));
                Elements maxs = doc.select("#" + columnsMap.get(MAX));

                String name = names.first().ownText().trim();
                String cnt = cnts.first().ownText().trim();
                String err = errs.first().ownText().trim();
                String avg = avgs.first().ownText().trim();
                String max = maxs.first().ownText().trim();
                String min = mins.first().ownText().trim();

                // avg: 5 secs 264 msecs
                long avgMillis = timeToNumber(avg);
                long minMillis = timeToNumber(min);
                long maxMillis = timeToNumber(max);

                Operation op = new Operation();
                op.name = name;
                op.avgResponseTime = avgMillis;
                op.minResponseTime = minMillis;
                op.maxResponseTime = maxMillis;
                op.messageCount = Long.parseLong(cnt.replaceAll("[^0-9]+", ""));
                op.errorCount = Long.parseLong(err.replaceAll("[^0-9]+", ""));
                op.debug = name + "/" + cnt + "/" + avg;

                if (!srv.operations.contains(op)) {
                    srv.operations.add(op);
                    found++;
                }

                line++;
            }

            if (found == 0) break;
            pageNo++;
        }
    }

    private String getSh1(int pageNo, Domain domain, String scanningMode) {
        String sh1 = domain.getConsoleUrl();
        if ("agg_view".equals(scanningMode)) {
            sh1 += "sbconsole.portal?_nfpb=true&_windowLabel=ServiceStatisticsSummaryPortlet" +
                    "&ServiceStatisticsSummaryPortlet_actionOverride=%2Fsvcmonitor%2FServiceSummaryPageOrSort" +
                    "&_pageLabel=Monitoring_Service_Summary&ServiceStatisticsSummaryPortlet_serviceMonSummary_LastReset_rowsPerPage=20" +
                    "&ServiceStatisticsSummaryPortlet_serviceMonSummary_LastReset_selected=&" +
                    "ServiceStatisticsSummaryPortletbottomGotoserviceMonSummary_LastReset=1&ServiceStatisticsSummaryPortletfrname=&" +
                    "ServiceStatisticsSummaryPortletmanaged_server=&ServiceStatisticsSummaryPortletsearch=Search&" +
                    "ServiceStatisticsSummaryPortletserviceType=&ServiceStatisticsSummaryPortletsrname=&" +
                    "ServiceStatisticsSummaryPortletsrpath=&ServiceStatisticsSummaryPortlettarget_table_id=serviceMonSummary_LastReset&" +
                    "ServiceStatisticsSummaryPortlettopGotoserviceMonSummary_LastReset=1&ServiceStatisticsSummaryPortletview_type=agg_view" +
                    "&ServiceStatisticsSummaryPortletpg_serviceMonSummary_LastReset=" + pageNo + "&ServiceStatisticsSummaryPortletrefreshSort=true&" +
                    "ServiceStatisticsSummaryPortletsortcol_serviceMonSummary_LastReset=pathColumn&ServiceStatisticsSummaryPortletsortord_serviceMonSummary_LastReset=asc";
        } else {
            sh1 += "sbconsole.portal?_nfpb=true&_windowLabel=ServiceStatisticsSummaryPortlet" +
                    "&ServiceStatisticsSummaryPortlet_actionOverride=%2Fsvcmonitor%2FServiceSummaryPageOrSort" +
                    "&_pageLabel=Monitoring_Service_Summary&ServiceStatisticsSummaryPortletserviceType=&ServiceStatisticsSummaryPortlet=" +
                    "&ServiceStatisticsSummaryPortletview_type=interval_view" +
                    "&ServiceStatisticsSummaryPortletmanaged_server=osb_server1&ServiceStatisticsSummaryPortletpg_serviceMonSummary_CurrAI=" + pageNo +
                    "&ServiceStatisticsSummaryPortletrefreshSort=true&ServiceStatisticsSummaryPortletsortcol_serviceMonSummary_CurrAI=nameColumn" +
                    "&ServiceStatisticsSummaryPortletsortord_serviceMonSummary_CurrAI=asc";
        }

        return sh1;
    }

    private Document getWithRetry(String lead, Domain domain) throws Exception {
        int timeout = 30000;
        Map<String, String> cookies = domain.getCookies();

        int tries = 3;
        while (true) {
            try {
                return Jsoup.connect(lead).cookies(cookies).timeout(timeout).get();
            } catch (Exception ex) {
                if (--tries == 0) throw ex;
                System.err.println("\nMmm... " + ex.getClass().getSimpleName() + ": " + ex.getMessage() + " - retrying once more...");
                timeout *= 2;
                continue;
            }
        }
    }

    private static String getSl(Domain domain, Service srv, String scanningMode) {
        String sl = domain.getConsoleUrl() + "sbconsole.portal?_nfpb=true&_pageLabel=Monitoring_Service_Statistics&";
        if (srv.type.equals("Biz")) {
            sl += "ServiceStatisticsPortletservice_to_monitor=BusinessService%24" + srv.name.replaceAll("/", "%24") + "&";
            sl += "ServiceStatisticsPortletservicetypeid=BusinessService&";
        } else {
            sl += "ServiceStatisticsPortletservice_to_monitor=ProxyService%24" + srv.name.replaceAll("/", "%24") + "&";
            sl += "ServiceStatisticsPortletservicetypeid=ProxyService&";
        }
        sl += "ServiceStatisticsPortletview_type=" + scanningMode + "&";
        sl += "ServiceStatisticsPortletmanaged_server=&";
        sl += "ServiceStatisticsPortletisnew=true";
        return sl;
    }

    // starts with /sbconsole/sbconsole.portal?_nfpb=true&_pageLabel=Monitoring_Service_Statistics&ServiceStatisticsPortletservice_to_monitor=
    // BusinessService%24BellTVServices%24BellTVProfile%24ChIP%24AccountManagement%24BizService%24AccountInformation_db&ServiceStatisticsPortletview_type=interval_view&ServiceStatisticsPortletreturntourl=%2Fsbconsole%2Fsbconsole.portal%3F_nfpb%3Dtrue%26_pageLabel%3DMonitoring_Service_Summary%26ServiceStatisticsSummaryPortletview_type%3Dinterval_view%26ServiceStatisticsSummaryPortletmanaged_server%3D%26ServiceStatisticsSummaryPortletrefreshSort%3Dtrue&ServiceStatisticsPortletmanaged_server=&ServiceStatisticsPortletisnew=true"
    private int collectLinks(Document doc, String wildcard) {
        int found = 0;
        Elements links = doc.select("a[href]");
        for (Element link : links) {
            String href = link.attr("href");
            // String title = link.attr("title");
            if (href.contains("/sbconsole/sbconsole.portal?_nfpb=true&_pageLabel=Monitoring_Service_Statistics&ServiceStatisticsPortletservice_to_monitor=")) {
                Service srv = new Service();
                srv.name = extractName(href);
                srv.type = extractType(href);
                srv.href = href;
                boolean added = addServiceToQueue(srv, wildcard);
                if (added) {
                    found++;
                }
            }
        }

        return found;
    }

    private String buildPattern(String input) {
        input = input.replaceAll("\\*", ".*");
        return ".*" + input + ".*";
    }

    private String extractName(String link) {
        int p1 = link.indexOf("ServiceStatisticsPortletservice_to_monitor=");
        int p2 = link.indexOf("&", p1 + 1);

        int p3 = link.indexOf("=", p1);

        String serviceName = "";
        if (link.contains("BusinessService%24")) {
            serviceName = link.substring(p3 + 1 + "BusinessService%24".length(), p2);
            serviceName = serviceName.replaceAll("%24", "/");
        } else {
            serviceName = link.substring(p3 + 1 + "ProxyService%24".length(), p2);
            serviceName = serviceName.replaceAll("%24", "/");
        }

        return serviceName;
    }

    private String extractType(String link) {
        if (link.contains("BusinessService%24")) {
            return "Biz";
        } else {
            return "Proxy";
        }
    }

    private Connection.Response getResponse(Domain domain, String scanningMode) throws IOException {
        String search = domain.getConsoleUrl() + "sbconsole.portal?_nfpb=true&_windowLabel=ServiceStatisticsSummaryPortlet&ServiceStatisticsSummaryPortlet_actionOverride=%2Fsvcmonitor%2FViewServiceSummary&_pageLabel=Monitoring_Service_Summary";
        return Jsoup.connect(search)
                .cookies(domain.getCookies())
                .data("ServiceStatisticsSummaryPortletview_type", scanningMode)
                .data("ServiceStatisticsSummaryPortletmanaged_server", "")
                .data("ServiceStatisticsSummaryPortletsrname", "")
                .data("ServiceStatisticsSummaryPortletsrpath", "")
                .data("ServiceStatisticsSummaryPortletserviceType", "")
                .data("ServiceStatisticsSummaryPortletfrname", "")
                .data("ServiceStatisticsSummaryPortletsearch", "Search")
                .data("ServiceStatisticsSummaryPortlettarget_table_id", "serviceMonSummary_LastReset")
                .data("ServiceStatisticsSummaryPortlet_serviceMonSummary_LastReset_selected", "")
                .data("ServiceStatisticsSummaryPortlet_serviceMonSummary_LastReset_rowsPerPage", "2000")
                .data("ServiceStatisticsSummaryPortlettopGotoserviceMonSummary_LastReset", "4")
                .data("ServiceStatisticsSummaryPortletbottomGotoserviceMonSummary_LastReset", "4")
                .method(Connection.Method.POST)
                .timeout(30000)
                .followRedirects(true)
                .execute();
    }

    private boolean addServiceToQueue(Service service, String wildcard) {
        System.out.println("Name: " + service.name);

        //found another link
        if (!knownServices.contains(service)) {
            if (wildcard == null || service.name.matches(wildcard)) {
                System.out.println("  Added.");
                linksToServices.add(service);
            }
            knownServices.add(service);
            return true;
        }
        return false;
    }

    private static long timeToNumber(String avg) {
        String dv = "0";
        String hv = "0";
        String mv = "0";
        String sv = "0";
        String msv = "0";

        int day = avg.indexOf(" day");
        int days = avg.indexOf(" days");
        if (days > 0) {
            dv = avg.substring(0, days);
            avg = avg.substring(days + " days".length());
        } else if (day > 0) {
            dv = avg.substring(0, day);
            avg = avg.substring(day + " day".length());
        }
        avg = avg.trim();
        
        int hour = avg.indexOf(" hour");
        int hours = avg.indexOf(" hours");
        if (hours > 0) {
            hv = avg.substring(0, hours);
            avg = avg.substring(hours + " hours".length());
        } else if (hour > 0) {
            hv = avg.substring(0, hour);
            avg = avg.substring(hour + " hour".length());
        }
        avg = avg.trim();

        int min = avg.indexOf(" min");
        int mins = avg.indexOf(" mins");
        if (mins > 0) {
            mv = avg.substring(0, mins);
            avg = avg.substring(mins + " mins".length());
        } else if (min > 0) {
            mv = avg.substring(0, min);
            avg = avg.substring(min + " min".length());
        }
        avg = avg.trim();

        int sec = avg.indexOf(" sec");
        int secs = avg.indexOf(" secs");
        if (secs > 0) {
            sv = avg.substring(0, secs);
            msv = avg.substring(secs + " secs".length()).replaceAll("[^0-9]+", "");
        } else if (sec > 0) {
            sv = avg.substring(0, sec);
            msv = avg.substring(sec + " sec".length()).replaceAll("[^0-9]+", "");
        } else {
            msv = avg.replaceAll("[^0-9]+", "");
        }

        if (mv.trim().length() == 0) mv = "0";
        if (sv.trim().length() == 0) sv = "0";
        if (msv.trim().length() == 0) msv = "0";

        return 	Long.parseLong(dv) * 24 * 60 * 60 * 1000l + 
        		Long.parseLong(hv) * 60 * 60 * 1000l + 
        		Long.parseLong(mv) * 60 * 1000l + 
        		Long.parseLong(sv) * 1000l + 
        		Long.parseLong(msv);
    }

    private static double parsePercent(String val) {
        // 10.52%
        val = val.replace(",", ".");
        if (val.endsWith("%")) val = val.substring(0, val.length() - 1);
        return Double.parseDouble(val);
    }

    private static Map<String, String> buildOperationColumnsMap(int line, String scanningMode) {
        HashMap<String, String> result = new HashMap<String, String>();
        String nameId;
        String countId;
        String errorsId;
        String avgId;
        String maxId;
        String minId;
        if ("agg_view".equals(scanningMode)) {
            nameId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_0";
            countId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_1";
            errorsId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_2";
            avgId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_5";
            maxId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_4";
            minId = "tableId_serviceMonOper_CurrAI_rowId_" + line + "_colId_3";
        } else {
            nameId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_0";
            countId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_1";
            errorsId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_2";
            avgId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_5";
            maxId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_4";
            minId = "tableId_serviceMonOper_LastReset_rowId_" + line + "_colId_3";
        }
        result.put(NAME, nameId);
        result.put(COUNT, countId);
        result.put(ERRORS, errorsId);
        result.put(AVG, avgId);
        result.put(MAX, maxId);
        result.put(MIN, minId);
        return result;
    }

    private static String getLead(Domain domain, Service srv, int pageNo, String scanningMode) {
        String lead = domain.getConsoleUrl() + "sbconsole.portal?_nfpb=true&_pageLabel=Monitoring_Service_Statistics&";
        if (srv.type.equals("Biz")) {
            lead += "ServiceStatisticsPortletservice_to_monitor=BusinessService%24" + srv.name.replaceAll("/", "%24") + "&";
            lead += "ServiceStatisticsPortletservicetypeid=BusinessService&";
        } else {
            lead += "ServiceStatisticsPortletservice_to_monitor=ProxyService%24" + srv.name.replaceAll("/", "%24") + "&";
            lead += "ServiceStatisticsPortletservicetypeid=ProxyService&";
        }
        lead += "ServiceStatisticsPortletview_type=" + scanningMode + "&ServiceStatisticsPortletmanaged_server=&ServiceStatisticsPortletstatistics_tab=tab_operations";
        lead += "&ServiceStatisticsPortletpg_serviceMonOper_CurrAI=" + pageNo;
        return lead;
    }
}
