package com.readosbstats;

import com.martiansoftware.jsap.JSAPResult;

import java.util.List;
import java.util.Map;

/**
 * Basic interface for all Scanner classes.
 */
public interface IScanner {

    void getServicesStatistics(Domain domain, List<Service> services, JSAPResult config, Map<String, String> options) throws Exception;

    void getServiceStatistics(Domain domain, Service service, JSAPResult config, Map<String, String> options) throws Exception;

    /**
     * Collects stats from all services provided in the given list
     *
     * @return
     */
    List<Service> collectAllServices(Domain domain, Map<String, String> options) throws Exception;
}
