package com.readosbstats;

import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPResult;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ScanManager {

    public void scan(JSAP jsap, JSAPResult config) throws Exception {
        disableCertificateChecks();
        URL url = config.getURL("console");
        String user = config.getString("user");
        String pass = config.getString("pass");

        if (url == null) {
            System.err.println("Error: Console URL is not specified; use -c option");
            ReadStats.reportError(jsap, config);
            return;
        }
        if (user == null || user.trim().isEmpty()) {
            System.err.println("Error: user id is not specified; use -u option");
            ReadStats.reportError(jsap, config);
            return;
        }

        Credentials credentials = getConnectionCredentials(user, pass, jsap, config);
        if (credentials == null) {
            return;
        }

        String host = url.getHost();
        int port = url.getPort();
        if (port < 0) {
            if (url.getProtocol().equals("https")) port = 443;
            else port = 80;
        }

        String consoleUrl = url.getProtocol() + "://" + host + ":" + port + "/sbconsole/";
        Domain domain = login(consoleUrl, credentials);
        if (domain != null) {
            Map<String, String> options = buildOptions(config);
            addOption(options, Constants.HOST, host);
            addOption(options, Constants.PORT, String.valueOf(port));
            IScanner scanner = getScanner(domain.getDomainVersion());
            List<Service> services = scanner.collectAllServices(domain, options);
            scanner.getServicesStatistics(domain, services, config, options);
        }

    }

    private Map<String, String> buildOptions(JSAPResult config) {
        Map<String, String> options = new HashMap<String, String>();
        String wildcard = config.getString("wildcard");
        options.put(Constants.WILDCARD, wildcard);
        Boolean isForCurrentPeriod = config.getBoolean("currentperiod");
        if (isForCurrentPeriod) {
            options.put(Constants.SCANNING_MODE, "interval_view");
        } else {
            options.put(Constants.SCANNING_MODE, "agg_view");
        }

        return options;
    }

    private void addOption(Map<String, String> options, String key, String value) {
        options.put(key, value);
    }

    private IScanner getScanner(DomainVersion domainVersion) {
        if (DomainVersion.OSB_V_11.equals(domainVersion)) {
            return new Osb11HTMLScanner();
        } else if (DomainVersion.OSB_V_12.equals(domainVersion)) {
            //TODO add logic to return scanner for version 12 after it will be implemented
            return null;
        } else {
        	return new Osb11HTMLScanner();
        }
    }

    /**
     * Try to login to OSB console and build {@link Domain}
     * instance
     *
     * @return {@link Domain} instances with all WLS domain information
     */
    //For now assume that all supported version of OSB have save logic for login
    private Domain login(String console, Credentials credentials) throws IOException {
        System.out.println("Connecting to " + console + " as user " + credentials.getUsername());
        String securityCheckUrl = console + "j_security_check";
        System.out.println("  Security Check: " + securityCheckUrl);

        Connection.Response response =
                Jsoup.connect(securityCheckUrl)
                        .data("j_username", credentials.getUsername())
                        .data("j_password", credentials.getPassword())
                        .method(Connection.Method.POST)
                        .timeout(30000)
                        .followRedirects(true)
                        .execute();
        String name = getDomainName(response.parse());
        DomainVersion domainVersion = getDomainVersion(response.parse());
        Map<String, String> cookies = response.cookies();
        
        Domain domain = new Domain(name, domainVersion, cookies, console);
//        if (isDomainValid(domain)) {
//            return domain;
//        }

        return domain;
    }

    private void disableCertificateChecks() throws Exception {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkServerTrusted(X509Certificate[] arg0, String arg1) {
                    }

                    public void checkClientTrusted(X509Certificate[] arg0, String arg1) {
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    private String getDomainName(Document document) {
        Elements elements = document.getElementsByClass("welcome_toplinks");
        if (elements != null && elements.size() > 0) {
            Elements subElements = elements.first().getElementsByTag("strong");
            if (subElements != null && subElements.size() > 0) {
                return subElements.first().text();
            }
        }
        return "";
    }

    private DomainVersion getDomainVersion(Document document) {
        Elements elements = document.getElementsByClass("Oracle_mastHead");
        if (elements != null && !elements.isEmpty()) {
            String ver = elements.first().text();
            for (DomainVersion domainVersion : DomainVersion.values()) {
                if (domainVersion.getVersion().equals(ver)) {
                    return domainVersion;
                }

            }
        }
        return null;
    }

    private Credentials getConnectionCredentials(String user, String pass, JSAP jsap, JSAPResult config) {
        if (user.startsWith("@")) {
            String absFilePath = "?";
            try {
                absFilePath = user.replace("@", "");
                File file = new File(absFilePath);
                FileInputStream fis = new FileInputStream(file);
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                String[] credentialsArray = br.readLine().split(":");
                if (credentialsArray.length != 2) {
                    System.err.println("Couldn't read username/password from" + absFilePath + ". The format of the file must be username:password in one line.");
                    ReadStats.reportError(jsap, config);
                    return null;
                }
                user = credentialsArray[0];
                pass = credentialsArray[1];
                br.close();
            } catch (Exception e) {
                System.err.println("Couldn't read username/password from" + absFilePath + ". " + e.getMessage());
                ReadStats.reportError(jsap, config);
                return null;
            }
        } else {
            if (pass == null || pass.trim().isEmpty()) {
                pass = System.getenv("ROS_PWD");
                if (pass == null || pass.trim().isEmpty()) {
                    System.err.println("Error: user password is not specified; use -p option");
                    ReadStats.reportError(jsap, config);
                    return null;
                }
            }
        }
        return new Credentials(user, pass);
    }

    private boolean isDomainValid(Domain domain) {
        return domain.getName() != null && !domain.getName().isEmpty() && domain.getDomainVersion() != null
                && domain.getCookies() != null && domain.getCookies().size() >= 1;


    }
}
