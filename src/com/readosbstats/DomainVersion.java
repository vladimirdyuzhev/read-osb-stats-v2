package com.readosbstats;

/**
 * Contains supported version of OSB
 */
public enum DomainVersion {

    OSB_V_11(" Service Bus 11gR1"), OSB_V_12("");

    private String version;

    DomainVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
