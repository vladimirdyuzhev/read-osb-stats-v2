package com.readosbstats;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Snapshot {
    public Date date;
    public String consoleUrl = "";
    public String domainName = "";
    public List<Service> services = new ArrayList<Service>();

    @Override
    public String toString() {
        return "Snapshot [date=" + date + ", consoleUrl=" + consoleUrl + ", domainName=" + domainName + "]";
    }
}
