package com.readosbstats;

import java.io.File;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.martiansoftware.jsap.JSAPResult;

public class Writer {

    public static String MARKER_SERVICE = "srv";
    public static String MARKER_TYPE = "typ";
    public static String MARKER_OPERATION = "op";

    public static String MARKER_BEGIN = "#";
    public static String MARKER_MESSAGES = "msgs";
    public static String MARKER_ERRORS = "errs";
    public static String MARKER_MIN = "min";
    public static String MARKER_AVG = "avg";
    public static String MARKER_MAX = "max";
    public static String MARKER_FAILOVER = "fovr";
    public static String MARKER_SUCCESS = "succ";
    public static String MARKER_FAILURE = "fail";
    public static String MARKER_CACHE_HIT = "chit";
    public static String MARKER_CACHE_RATIO = "crat";
    public static String MARKER_THROTTLING_MIN = "mint";
    public static String MARKER_THROTTLING_AVG = "avgt";
    public static String MARKER_THROTTLING_MAX = "maxt";

    public static void output(List<Snapshot> snapshots, File fn, JSAPResult config) throws Exception {
        output(snapshots, fn, config, true);
    }

    public static void output(List<Snapshot> snapshots, File fn, JSAPResult config, boolean isMergeable) throws Exception {
        boolean nonZero = config.getBoolean("nonzero");
        boolean serviceLevelOnly = config.getBoolean("servicelevel");

        // fields to output
        String fs = config.getString("fields");
        if (fs == null) fs = "msgs,errs,min,avg,max,fovr,succ,fail,chit,crat,mint,avgt,maxt";
        String[] fields = fs.split(",");

        // build the full list of all services sorted by name
        List<Service> known = new ArrayList<Service>();
        for (Snapshot ss : snapshots) {
            for (Service service : ss.services) {
                if (!known.contains(service)) known.add(service);
            }
        }
        Collections.sort(known);

        // get list of snapshots sorted
        Collections.sort(snapshots,new Comparator<Snapshot>() {
			@Override
			public int compare(Snapshot o1, Snapshot o2) {
				int c = o1.date.compareTo(o2.date);
				if( c != 0 ) return c;
				c = o1.domainName.compareTo(o2.domainName);
				return c;
			}
		});

        PrintWriter pw = new PrintWriter(fn);

        if (!isMergeable) {
            pw.write("NOT MERGEABLE");
        }

        // headers 1
        pw.write(",,,");
        for (Snapshot ss: snapshots) {
        	Date d = ss.date;
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            pw.printf("%s", sdf.format(d));
            for (String f : fields) {
                pw.write(",");
            }
        }
        pw.write("\n");
        pw.write(",,,");
        for (Snapshot ss: snapshots) {
            if (ss != null) {
            	if( ss.domainName != null && !ss.domainName.isEmpty() ) {
            		pw.write(ss.domainName);
            	} else {
            		pw.write(ss.consoleUrl);
            	}
            } else {
                pw.write("?");
            }

            for (String f : fields) {
                pw.write(",");
            }
        }
        pw.write("\n");

        // headers 2
        pw.write("Service,Type,Operation,");
        for (Snapshot ss: snapshots) {
            for (String f : fields) {
                if (MARKER_AVG.equals(f)) pw.write("Avg(ms),");
                else if (MARKER_CACHE_HIT.equals(f)) pw.write("Cache Hits,");
                else if (MARKER_CACHE_RATIO.equals(f)) pw.write("Cache Hits(%),");
                else if (MARKER_ERRORS.equals(f)) pw.write("Errors,");
                else if (MARKER_FAILOVER.equals(f)) pw.write("Failovers,");
                else if (MARKER_FAILURE.equals(f)) pw.write("Failures(%),");
                else if (MARKER_MAX.equals(f)) pw.write("Max(ms),");
                else if (MARKER_MESSAGES.equals(f)) pw.write("Messages,");
                else if (MARKER_MIN.equals(f)) pw.write("Min(ms),");
                else if (MARKER_SUCCESS.equals(f)) pw.write("Success(%),");
                else if (MARKER_THROTTLING_AVG.equals(f)) pw.write("Avg Throttling(ms),");
                else if (MARKER_THROTTLING_MAX.equals(f)) pw.write("Max Throttling(ms),");
                else if (MARKER_THROTTLING_MIN.equals(f)) pw.write("Min Throttling(ms),");
            }
        }
        pw.write("\n");

        // iterate all services
        for (Service srv : known) {
            // if the service has no messages, skip it if -z is provided
            boolean isZero = isZero(snapshots, srv);
            if (nonZero && isZero) continue;

            if( !serviceLevelOnly ) pw.println();
            
            pw.printf("%s,%s,%s,", srv.name, srv.type, "(Overall)");

            for (Snapshot ss: snapshots) {
            	Date date = ss.date;
                Service currentService = getServiceByDate(ss, date, srv);

                if (currentService == null) {
                    for (String f : fields) {
                        pw.print(",");
                    }
                } else {
                    for (String f : fields) {
                        if (MARKER_AVG.equals(f)) pw.printf("%d,", currentService.avgResponseTime);
                        else if (MARKER_CACHE_HIT.equals(f)) pw.printf("%d,", currentService.hitCount);
                        else if (MARKER_CACHE_RATIO.equals(f)) pw.printf("%.3f,", currentService.hitRatio);
                        else if (MARKER_ERRORS.equals(f)) pw.printf("%d,", currentService.errorCount);
                        else if (MARKER_FAILOVER.equals(f)) pw.printf("%d,", currentService.failoverCount);
                        else if (MARKER_FAILURE.equals(f)) pw.printf("%.3f,", currentService.failureRatio);
                        else if (MARKER_MAX.equals(f)) pw.printf("%d,", currentService.maxResponseTime);
                        else if (MARKER_MESSAGES.equals(f)) pw.printf("%d,", currentService.messageCount);
                        else if (MARKER_MIN.equals(f)) pw.printf("%d,", currentService.minResponseTime);
                        else if (MARKER_SUCCESS.equals(f)) pw.printf("%.3f,", currentService.successRatio);
                        else if (MARKER_THROTTLING_AVG.equals(f)) pw.printf("%d,", currentService.avgThrottlingTime);
                        else if (MARKER_THROTTLING_MAX.equals(f)) pw.printf("%d,", currentService.maxThrottlingTime);
                        else if (MARKER_THROTTLING_MIN.equals(f)) pw.printf("%d,", currentService.minThrottlingTime);
                    }
                }
            }

            pw.print("\n");

            if (serviceLevelOnly) continue;

            List<Operation> knownOperations = collectOperations(snapshots, srv);
            Collections.sort(knownOperations);

            for (Operation op : knownOperations) {
                boolean isZeroOperation = isZero(snapshots, srv, op);

                if (nonZero && isZeroOperation) continue;

                pw.printf("%s,%s,%s,", srv.name, srv.type, op.name);

                for (Snapshot ss: snapshots) {
                	Date opDate = ss.date;
                    Service currentService = getServiceByDate(ss, opDate, srv);
                    Operation foundOp = getOperation(currentService, op);

                    if (foundOp == null) {
                        for (String f : fields) {
                            pw.print(",");
                        }
                    } else {
                        for (String f : fields) {
                            if (MARKER_AVG.equals(f)) pw.printf("%d,", foundOp.avgResponseTime);
                            else if (MARKER_ERRORS.equals(f)) pw.printf("%d,", foundOp.errorCount);
                            else if (MARKER_MAX.equals(f)) pw.printf("%d,", foundOp.maxResponseTime);
                            else if (MARKER_MESSAGES.equals(f)) pw.printf("%d,", foundOp.messageCount);
                            else if (MARKER_MIN.equals(f)) pw.printf("%d,", foundOp.minResponseTime);
                            else pw.printf("%s,", "");
                        }
                    }
                }

                pw.printf("\n");
            }
        }

        // columns markers
        pw.write("\n");
        pw.write(MARKER_SERVICE + "," + MARKER_TYPE + "," + MARKER_OPERATION + ",");
        for (Snapshot ss: snapshots) {
            pw.write(MARKER_BEGIN);
            for (String f : fields) {
                pw.write(f + ",");
            }
        }
        pw.write("\n");
        pw.close();
    }

    private static Operation getOperation(Service currentService, Operation op) {
        if (currentService == null) return null;
        for (Operation found : currentService.operations) {
            if (found.equals(op)) return found;
        }
        return null;
    }

    private static boolean isZero(List<Snapshot> snapshots, Service srv, Operation op) {
        for (Snapshot ss : snapshots) {
            for (Service service : ss.services) {
                if (service.equals(srv)) {
                    for (Operation fop : service.operations) {
                        if (fop.equals(op)) {
                            if (fop.messageCount > 0) return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    private static List<Operation> collectOperations(List<Snapshot> snapshots, Service srv) {
        List<Operation> knownOperations = new ArrayList<Operation>();

        // list of operations may be different in different snapshots (e.g. after updated WSDL)
        for (Snapshot ss : snapshots) {
            for (Service service : ss.services) {
                if (service.equals(srv)) {
                    // found it
                    for (Operation op : service.operations) {
                        if (!knownOperations.contains(op)) knownOperations.add(op);
                    }
                }
            }
        }

        return knownOperations;
    }

    /**
     * Finds a service under specific date
     */
    private static Service getServiceByDate(Snapshot ss, Date date, Service srv) {
        for (Service service : ss.services) {
            if (service.equals(srv)) {
                return service;
            }
        }
        return null;
    }

    /*
     * Returns true is none of the service readings has any messages
     */
    private static boolean isZero(List<Snapshot> snapshots, Service srv) {
        for (Snapshot ss : snapshots) {
            Service found = null;
            for (Service service : ss.services) {
                if (service.equals(srv)) {
                    found = service;
                    break;
                }
            }

            if (found == null) continue;

            if (found.messageCount > 0) {
                // got some messages
                return false;
            }
        }

        return true;
    }
}
